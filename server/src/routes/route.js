const express = require("express");
const userRoutes = require("./user");
const authRoutes = require("./auth");
const router = express.Router();

const { passportJWT } = require("../middlewares/auth");

router.use("/user", passportJWT, userRoutes);
router.use("/auth", authRoutes);

module.exports = router;
