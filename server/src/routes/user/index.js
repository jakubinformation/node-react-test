const express = require("express");
const router = express.Router();
const user = require("../../controllers/user");

router.get("/all", user.getUsers);
router.put("/:userId", user.updateUser);
router.delete("/:userId", user.deleteUser);
module.exports = router;
