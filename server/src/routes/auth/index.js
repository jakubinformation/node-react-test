const express = require("express");
const router = express.Router();
const { validate } = require("express-validation");
const { registerUser, loginUser } = require("../../middlewares/validation");
const { signin, signup } = require("../../controllers/authentication");
const { passportSignIn } = require("../../middlewares/auth");

router.post("/signup", validate(registerUser, {}, {}), signup);
router.post("/signin", validate(loginUser, {}, {}), passportSignIn, signin);
// routes.route('/refreshtoken').post(refreshToken);
// routes.route('/verifyemail').post(verifyEmail);
// routes.route('/resetpassword').post(resetPassword);
module.exports = router;
