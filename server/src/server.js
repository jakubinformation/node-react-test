const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
const config = require("./config");
const cors = require("cors");
const app = express();
const PORT = process.env.PORT || 8000;
require("dotenv").config({
  path: path.join(__dirname, "../../.env"),
});

const routes = require("./routes/route.js");

mongoose.connect(config.database.url).then(() => {
  console.log("Connected to the Database successfully");
});

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use("/api/v1/", routes);

app.listen(PORT, () => {
  console.log("Server is listening on Port:", PORT);
});
