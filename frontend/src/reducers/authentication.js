import { actions, TOKEN } from '../constants';

const initialState = {
  isSubmitting: false,
  isFailed: false,
};

const actionMap = {
  [actions.LOGIN_REQUEST]: state => ({ ...state, isSubmitting: true, error: null }),
  [actions.LOGIN_SUCCESS]: (state, { result: { data } }) => {
    localStorage.setItem(TOKEN, JSON.stringify(data.token));
    return ({ ...state, isSubmitting: false });
  },
  [actions.LOGIN_FAILURE]: state => ({ ...state, isFailed: true, isSubmitting: false }),

  [actions.REGISTER_REQUEST]: state => ({ ...state, isSubmitting: true, error: null }),
  [actions.REGISTER_SUCCESS]: (state, { result: { data } }) => {
    localStorage.setItem(TOKEN, JSON.stringify(data.token));
    return ({ ...state, isSubmitting: false });
  },
  [actions.REGISTER_FAILURE]: state => ({ ...state, isFailed: true, isSubmitting: false }),

  [actions.LOGOUT]: state => {
    localStorage.removeItem(TOKEN);
    return state;
  },
};

export default (state = initialState, action) => {
  if (actionMap[action.type]) {
    return actionMap[action.type](state, action);
  }

  return state;
};

export const selectIsFailed = state => state.authentication.isFailed;
export const selectIsSubmitting = state => state.authentication.isSubmitting;
