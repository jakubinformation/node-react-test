import { unionBy } from 'lodash';
import { actions } from '../constants';

const initialState = {
  isLoaded: false,
  isFailed: false,
  users: [],
};

const actionMap = {
  [actions.GET_USERS_REQUEST]: state => ({ ...state, isLoaded: false }),
  [actions.GET_USERS_SUCCESS]: (state, { result: { data: { data } } }) => ({ ...state, isLoaded: true, users: data }),
  [actions.GET_USERS_FAILURE]: state => ({ ...state, isLoaded: false }),

  [actions.DELETE_USER_REQUEST]: state => ({ ...state, isFailed: false }),
  [actions.DELETE_USER_SUCCESS]: (state, { result: { data } }) => ({ ...state, isFailed: false, users: data.filter(item => item.id !== data.id) }),
  [actions.DELETE_USER_FAILURE]: state => ({ ...state, isFailed: true }),

  [actions.CREATE_USER_REQUEST]: state => ({ ...state, isFailed: false }),
  [actions.CREATE_USER_SUCCESS]: (state, { result: { data } }) => ({ ...state, isLoaded: false, users: unionBy(state.users, [data], 'id') }),
  [actions.CREATE_USER_FAILURE]: state => ({ ...state, isFailed: true }),

  [actions.UPDATE_USER_REQUEST]: state => ({ ...state, isFailed: false }),
  [actions.UPDATE_USER_SUCCESS]: (state, { result: { data } }) => ({ ...state, isFailed: false, users: data.concat([state.users]) }),
  [actions.UPDATE_USER_FAILURE]: state => ({ ...state, isFailed: true }),
};

export default (state = initialState, action) => {
  if (actionMap[action.type]) {
    return actionMap[action.type](state, action);
  }

  return state;
};

export const selectIsFailed = state => state.user.isFailed;
export const selectIsLoaded = state => state.user.isLoaded;
export const selectUsers = state => state.user.users;
