import authentication from './authentication';
import user from './user';

export default {
  authentication,
  user,
};
