import actions from './actions';
import paths from './paths';

export {
  actions,
  paths,
};

export const TOKEN = 'auth';
export const toastOptions = {
  className: 'custom-success-toast',
  position: 'bottom-center',
  hideProgressBar: true,
};
export const toastError = {
  className: 'custom-error-toast',
  position: 'bottom-center',
  hideProgressBar: true,
};
