import _api from './api';
import _client from './client';

export const build = (path, ...params) => {
  params.reverse();
  return path.replace(/(:\w+)\??/g, () => params.pop());
};

export const api = { v1: _api };
export const client = _client;

export default {
  api: { v1: _api },
  client,
  build,
};
