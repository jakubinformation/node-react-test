export default {
  LOGIN: "/api/v1/auth/signin/",
  SIGNUP: "/api/v1/auth/signup/",
  USERS: "/api/v1/user/all/",
  USER_DETAILS: "/api/v1/user/:id/",
};
