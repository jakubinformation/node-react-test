export default {
  BASE: '/',
  LOGIN: '/login',
  SIGNUP: '/signup',
  HOME: '/home',
};
