import axios from "axios";
import { toast } from "react-toastify";
import { actions, TOKEN, toastOptions, toastError } from "../constants";
import authentication from "../actions/authentication";

const NOTIFICATION_NOT_REQUIRED_REQUESTS = [actions.LOGOUT];

const sendToastNotification = (toastType, requestType, errorMessage) => {
  if (toastType === "success") {
    toast.success("Success!", toastOptions);
  }
  if (toastType === "failure") {
    toast.error(errorMessage, toastError);
  }
};

export default () =>
  ({ dispatch, getState }) =>
  (next) =>
  (action) => {
    if (typeof action === "function") {
      return action(dispatch, getState);
    }
    const callAPIAction = action[actions.CALL_API];
    if (typeof callAPIAction === "undefined" || !callAPIAction.promise) {
      return next(action);
    }

    const { promise, types, ...rest } = callAPIAction;
    const [REQUEST, SUCCESS, FAILURE] = types;

    let token = null;
    try {
      token = JSON.parse(localStorage.getItem(TOKEN));
    } catch (e) {
      console.log(e);
    }
    const headers = token ? { Authorization: token } : null;
    next({ ...rest, type: REQUEST });

    return promise(
      axios.create({ headers, baseURL: "http://localhost:8000" }),
      dispatch
    ).then(
      (result) => {
        if (!NOTIFICATION_NOT_REQUIRED_REQUESTS.includes(REQUEST))
          sendToastNotification("success", SUCCESS);
        setTimeout(() => Promise.resolve(), 500);
        return next({ ...rest, result, type: SUCCESS });
      },
      (error) => {
        if (
          !NOTIFICATION_NOT_REQUIRED_REQUESTS.includes(REQUEST) &&
          error.response &&
          error.response.status !== 401 &&
          !axios.isCancel(error)
        ) {
          const errorMessage =
            typeof error.response.data === "string"
              ? error.message
              : Object.values(error.response.data)
                  .reduce((acc, val) => acc.concat(val), [])
                  .pop();
          sendToastNotification("failure", FAILURE, errorMessage);
        }
        next({ ...rest, error, type: FAILURE });
        const authError = error.toJSON().status === 401;
        console.log(error.toJSON().status, authError);
        if (authError && REQUEST !== actions.LOGIN_REQUEST) {
          console.log(authentication);
          // return dispatch(authentication.logout());
        }
        return authError ? null : Promise.reject(error);
      }
    );
  };
