import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import middleware from './middleware';
import reducers from '../reducers';

const store = createStore(combineReducers(reducers), applyMiddleware(thunk, middleware()));
export default store;
