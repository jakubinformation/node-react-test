import { actions, paths, TOKEN } from '../constants';

export default {
  register: (credentials) => ({
    [actions.CALL_API]: {
      types: [
        actions.REGISTER_REQUEST,
        actions.REGISTER_SUCCESS,
        actions.REGISTER_FAILURE,
      ],
      promise: client => client.post(paths.api.v1.SIGNUP, credentials),
    },
  }),

  login: (credentials) => ({
    [actions.CALL_API]: {
      types: [
        actions.LOGIN_REQUEST,
        actions.LOGIN_SUCCESS,
        actions.LOGIN_FAILURE,
      ],
      promise: client => client.post(paths.api.v1.LOGIN, credentials),
    },
  }),
  logout: () => {
    localStorage.removeItem(TOKEN);
    window.location.href = '/login';
    return {
      type: actions.LOGOUT,
    };
  },
};
