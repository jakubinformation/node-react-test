import { actions, paths } from '../constants';

export default {
  getUsers: () => ({
    [actions.CALL_API]: {
      types: [
        actions.GET_USERS_REQUEST,
        actions.GET_USERS_SUCCESS,
        actions.GET_USERS_FAILURE,
      ],
      promise: client => client.get(paths.api.v1.USERS),
    },
  }),

  createUser: (data) => ({
    [actions.CALL_API]: {
      types: [
        actions.CREATE_USER_REQUEST,
        actions.CREATE_USER_SUCCESS,
        actions.CREATE_USER_FAILURE,
      ],
      promise: client => client.post(paths.api.v1.USERS, data),
    },
  }),

  deleteUser: (id) => ({
    [actions.CALL_API]: {
      types: [
        actions.DELETE_USER_REQUEST,
        actions.DELETE_USER_SUCCESS,
        actions.DELETE_USER_FAILURE,
      ],
      promise: client => client.delete(paths.build(paths.api.v1.USER_DETAILS, id)),
    },
  }),

  updateUser: (id, data) => ({
    [actions.CALL_API]: {
      types: [
        actions.UPDATE_USER_REQUEST,
        actions.UPDATE_USER_SUCCESS,
        actions.UPDATE_USER_FAILURE,
      ],
      promise: client => client.put(paths.build(paths.api.v1.USER_DETAILS, id), data),
    },
  }),
};
