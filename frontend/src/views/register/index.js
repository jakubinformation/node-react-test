import React from "react";
import { withRouter, Link } from "react-router-dom";
import * as Yup from "yup";
import { Form, Formik } from "formik";
import { Input, Button, FormFeedback, Row, FormGroup } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import actions from "actions";
import { paths } from "constants";
import { selectIsSubmitting, selectIsFailed } from "reducers/authentication";
import "../login/styles.css";

const Register = ({ history: { push } }) => {
  const dispatch = useDispatch();
  const isSubmitting = useSelector(selectIsSubmitting);
  const isFailed = useSelector(selectIsFailed);
  const { register } = actions.authentication;

  const handleSubmit = (values) => {
    const { email, password } = values;
    dispatch(register({ email, password })).then(() => {
      push(paths.client.HOME);
    });
  };

  const schema = Yup.object().shape({
    email: Yup.string().email("Email not valid").required("Email is required"),
    password: Yup.string().required("Password is required"),
    confirmPassword: Yup.string()
      .required("Please confirm your password")
      .oneOf([Yup.ref("password")], "Password do not match"),
  });

  return (
    <div className="container">
      <div className="wrapper">
        <h2 className="mb-5">Register</h2>
        <Formik
          initialValues={{ email: "", password: "", confirmPassword: "" }}
          onSubmit={handleSubmit}
          validationSchema={schema}
        >
          {({ values, handleChange, errors, touched }) => (
            <Form className="form-container">
              <Row>
                <FormGroup>
                  <Input
                    name="email"
                    placeholder="Email"
                    onChange={handleChange}
                    value={values.email}
                    invalid={errors.email && touched.email}
                  />
                  <FormFeedback>{errors.email}</FormFeedback>
                </FormGroup>
              </Row>
              <Row>
                <FormGroup>
                  <Input
                    type="password"
                    name="password"
                    placeholder="Password"
                    onChange={handleChange}
                    value={values.password}
                    invalid={errors.password && touched.password}
                  />
                  <FormFeedback>{errors.password}</FormFeedback>
                </FormGroup>
              </Row>
              <Row>
                <FormGroup>
                  <Input
                    type="password"
                    name="confirmPassword"
                    placeholder="Confirm Password"
                    onChange={handleChange}
                    value={values.confirmPassword}
                    invalid={errors.confirmPassword && touched.confirmPassword}
                  />
                  <FormFeedback>{errors.confirmPassword}</FormFeedback>
                </FormGroup>
              </Row>
              {isFailed && (
                <>
                  <Input type="hidden" invalid={isFailed} />
                  <FormFeedback>
                    Sorry, but your request was failed.
                  </FormFeedback>
                </>
              )}
              <Button
                type="submit"
                className="btn btn-primary"
                disabled={isSubmitting}
              >
                Register
              </Button>
              <Link to={paths.client.LOGIN}>Go to login</Link>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

Register.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

export default withRouter(Register);
