import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Container } from 'reactstrap';
import actions from 'actions';
import PropTypes from 'prop-types';
import { selectUsers } from 'reducers/user';

const Home = ({ history: { push } }) => {
  const dispatch = useDispatch();
  const { logout } = actions.authentication;
  const { getUsers } = actions.user;
  const users = useSelector(selectUsers);

  useEffect(() => {
    dispatch(getUsers());
  }, []);

  const handleLogout = () => {
    dispatch(logout());
    push('/');
  };

  return (
    <Container>
      <h2 className="mt-lg-5">This is home page!</h2>
      <ul>
        {users.map((item, key) => (
          <li key={key}>{item.email}</li>
        ))}
      </ul>
      <a className="mt-lg-5" onClick={handleLogout}>Logout</a>
    </Container>
  );
}

Home.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
};

export default withRouter(Home);
