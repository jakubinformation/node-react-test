import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'reactstrap';
import { paths } from 'constants';

const Dashboard = () => (
  <Container>
    <h2 className="mt-lg-5">Welcome to our site!</h2>
    <p>Please click login or signup button below to get in our home page.</p>
    <Link to={paths.client.LOGIN}>Login</Link><br />
    <Link to={paths.client.SIGNUP}>Signup</Link>
  </Container>
);

export default Dashboard;
