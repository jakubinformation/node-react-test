import React from "react";
import { withRouter, Link } from "react-router-dom";
import PropTypes from "prop-types";
import * as Yup from "yup";
import { Form, Formik } from "formik";
import { Input, Button, FormFeedback, Row, FormGroup } from "reactstrap";
import actions from "actions";
import { useDispatch, useSelector } from "react-redux";
import { selectIsFailed, selectIsSubmitting } from "reducers/authentication";
import { paths } from "constants";
import "./styles.css";

const Login = ({ history: { push } }) => {
  const dispatch = useDispatch();
  const isFailed = useSelector(selectIsFailed);
  const isSubmitting = useSelector(selectIsSubmitting);
  const { login } = actions.authentication;

  const handleSubmit = (values) => {
    dispatch(login(values)).then(() => {
      push("/home");
    });
  };

  const schema = Yup.object().shape({
    email: Yup.string().email("Email not valid").required("Email is required"),
    password: Yup.string().required("Password is required"),
  });

  return (
    <div className="container">
      <div className="wrapper">
        <h2 className="mb-5">Login</h2>
        <Formik
          initialValues={{ email: "", password: "" }}
          onSubmit={handleSubmit}
          validationSchema={schema}
        >
          {({ values, handleChange, errors, touched }) => (
            <Form className="form-container">
              <Row>
                <FormGroup>
                  <Input
                    name="email"
                    placeholder="Email"
                    onChange={handleChange}
                    value={values.email}
                    invalid={errors.email && touched.email}
                  />
                  <FormFeedback>{errors.email}</FormFeedback>
                </FormGroup>
              </Row>
              <Row>
                <FormGroup>
                  <Input
                    type="password"
                    name="password"
                    placeholder="Password"
                    onChange={handleChange}
                    value={values.password}
                    invalid={errors.password && touched.password}
                  />
                  <FormFeedback>{errors.password}</FormFeedback>
                </FormGroup>
              </Row>
              {isFailed && (
                <>
                  <Input type="hidden" invalid={isFailed} />
                  <FormFeedback>
                    The email or password you entered is incorrect!
                  </FormFeedback>
                </>
              )}
              <Button
                type="submit"
                className="btn btn-primary"
                disabled={isSubmitting}
              >
                Login
              </Button>
              <Link to={paths.client.SIGNUP}>Go to register</Link>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

Login.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

export default withRouter(Login);
