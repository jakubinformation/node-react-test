import { Provider } from 'react-redux';
import store from './store';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Dashboard, Home, Login, Register } from './views';
import './App.css';
import { AuthorizedRoute } from './utils';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => (
  <div className="App">
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={() => <Dashboard />} />
          <Route exact path="/login" component={() => <Login />} />
          <Route exact path="/signup" component={() => <Register />} />
          <AuthorizedRoute exact path="/home" component={() => <Home />} />
        </Switch>
      </BrowserRouter>
    </Provider>
  </div>
);

export default App;
