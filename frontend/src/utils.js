import { Redirect, Route } from 'react-router-dom';
import { paths, TOKEN } from 'constants';
import PropTypes from 'prop-types';

export const AuthorizedRoute = (props) => {
  const token = localStorage.getItem(TOKEN);
  const { component: Component, ...rest } = props;
  return (
    <Route
      {...rest}
      render={(props) => token ? <Component {...props} /> : <Redirect to={paths.client.LOGIN} />}
    />
  );
};

AuthorizedRoute.propTypes = {
  component: PropTypes.elementType.isRequired,
};
